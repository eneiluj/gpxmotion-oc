OC.L10N.register(
    "gpxmotion",
    {
    "View in GpxMotion" : "Zobacz w GpxMotion",
    "Edit with GpxMotion" : "Edytuj w GpxMotion",
    "no vehicle" : "brak pojazdu",
    "plane" : "samolot",
    "car" : "samochód",
    "foot" : "stopa",
    "bike" : "rower",
    "train" : "pociąg",
    "bus" : "autobus",
    "Tile server \"{ts}\" has been deleted" : "Kafelka serwera \"{ts}\" została usunięta",
    "Failed to delete tile server \"{ts}\"" : "Nie udało się usunąć kafelki serwera \"{ts}\"",
    "Server name or server url should not be empty" : "Nazwa serwera lub adres url nie powinny być puste",
    "Impossible to add tile server" : "Nie udało się dodać kafelka serwera",
    "A server with this name already exists" : "Serwer o tej nazwie już istnieje",
    "Delete" : "Usuń",
    "Tile server \"{ts}\" has been added" : "Kafelek serwera \"{ts}\" został dodany",
    "Failed to add tile server \"{ts}\"" : "Nie udało się dodać kafelka serwera \"{ts}\"",
    "File successfully saved as" : "Plik zapisany pomyślnie jako",
    "Impossible to load this file. " : "Nie można załadować tego pliku. ",
    "Supported format : gpx" : "Wspierany format : gpx",
    "Load error" : "Błąd ładowania",
    "Number of tracks/routes" : "Liczba ścieżek/tras",
    "Vehicle" : "Pojazd",
    "Duration (sec)" : "Czas trwania (s)",
    "Section title" : "Tytuł sekcji",
    "Guess title from track/route name" : "Odgadnij tytuł od nazwy ścieżki/trasy",
    "Color" : "Kolor",
    "Description" : "Opis",
    "Picture URL" : "Adres URL zdjęcia",
    "Detail URL" : "Szczegóły URL",
    "Starting point title" : "Początkowy punkt tytułu",
    "Starting point description" : "Początkowy punkt opisu",
    "Starting point picture URL" : "Początkowy punkt adresu URL zdjęcia",
    "Starting point detail URL" : "Początkowy punkt szczegółu URL",
    "Remove section" : "Usuń sekcję",
    "Zoom on section" : "Przybliż sekcję",
    "Insert section before" : "Wstaw sekcję przed",
    "Insert section after" : "Wstaw sekcję po",
    "Impossible to write file" : "Nie można zapisać pliku",
    "Write access denied" : "Odmowa dostępu do zapisu",
    "Folder does not exist" : "Folder nie istnieje",
    "Folder write access denied" : "Odmowa dostępu do zapisu w folderze",
    "Bad file name, must end with \".gpx\"" : "Niepoprawna nazwa pliku, musi kończyć się na \".gpx\"",
    "Load file (gpx)" : "Wczytaj plik (gpx)",
    "There is nothing to save" : "Nie ma nic do zapisania",
    "Where to save" : "Gdzie zapisać",
    "Load and view animation file (gpx)" : "Załaduj i wyświetl animacje pliku (gpx)",
    "All sections synchronized" : "Wszystkie sekcje zsynchronizowane",
    "All sections" : "Wszystkie sekcje",
    "no title" : "brak tytułu",
    "Section" : "Sekcja",
    "Options" : "Opcje",
    "loop" : "pętla",
    "Legend" : "Legenda",
    "Pins" : "Pineski",
    "step" : "krok",
    "end" : "koniec",
    "Edit current file" : "Edytuj obecny plik",
    "Share current file" : "Udostępnij bieżący plik",
    "Public link to" : "Publiczny link do",
    "This public link will work only if \"{title}\" or one of its parent folder is shared in \"files\" app by public link without password" : "Ten publiczny link zadziała tylko wtedy, jeśli \"{title}\" lub jeden z jego folderów nadrzędnych jest udośtępniony w aplikacji \"files\" poprzez publiczny link bez hasła",
    "Load file" : "Załaduj plik",
    "Restrict animation to current view" : "Ogranicz animacje do bieżącego widoku",
    "Next section (n)" : "Następna sekcja (n)",
    "Previous section (p)" : "Poprzednia sekcja (p)",
    "Draw complete trip (g)" : "Narysuj kompletną podróże (g)",
    "Play/Pause animation (spacebar)" : "Odtwórz/Wstrzymaj animacje (spacebar)",
    "No animation data found in the GPX file" : "Nie znaleziono danych o animacji w pliku GPX",
    "Nothing to display" : "Brak danych do wyświetlenia",
    "More about" : "Więcej o",
    "Click for more details" : "Kliknij, aby uzyskać więcej szczegółów",
    "Step" : "Krok",
    "final" : "końcowy",
    "Ready to play" : "Gotowy do gry",
    "Load a file to display an animation" : "Załaduj plik aby wyświetlić animację",
    "left" : "lewo",
    "right" : "prawo",
    "Load and save files" : "Załaduj i zapisz pliki",
    "About GpxMotion" : "O GpxMotion",
    "Load and view file" : "Załaduj i zobacz plik",
    "Load and edit file" : "Załaduj i edytuj plik",
    "Save" : "Zapisz",
    "File name" : "Nazwa pliku",
    "Choose directory and save" : "Wybierz katalog, a następnie zapisz",
    "Preview current animation" : "Podgląd bieżącej animacji",
    "Use real time proportions" : "Użyj proporcji w czasie rzeczywistym",
    "Play all sections simultaneously" : "Włącz wszystkie sekcje jednocześnie",
    "Play all sections synchronized in real time" : "Włącz wszystkie sekcje zsynchronizowanie w czasie rzeczywistym",
    "Total duration (sec)" : "Całkowity czas trwania (s)",
    "Clear animation sections" : "Czyste sekcje animacji",
    "Add animation section" : "Dodaj sekcje animacji",
    "loading file" : "ładowanie pliku",
    "exporting file to gpx" : "eksportowanie pliku do gpx",
    "saving file" : "zapisywanie pliku",
    "Clear sections before loading" : "Wyczyść sekcje przed załądowaniem",
    "Custom tile servers" : "Niestandardowe kafelki serwerowe",
    "Server name" : "Nazwa serwera",
    "For example : my custom server" : "Na przykład : mój niestandardowy serwer",
    "Server url" : "Adres url serwera",
    "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png" : "Na przykład : http://tile.server.org/cycle/{z}/{x}/{y}.png",
    "Min zoom (1-20)" : "Minimalne powiększenie (1-20)",
    "Max zoom (1-20)" : "Maksymalne powiększenie (1-20)",
    "Add" : "Dodaj",
    "Your tile servers" : "Twoje kafelki serwerów",
    "Custom overlay tile servers" : "Niestandardowe nakładki kafelek na serwery",
    "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png" : "Na przykład : http://overlay.server.org/cycle/{z}/{x}/{y}.png",
    "Transparent" : "Przezroczysty",
    "Opacity (0.0-1.0)" : "Przezroczystość (0.0-1.0)",
    "Your overlay tile servers" : "Twoje nakładki kafelek na serwerach",
    "Custom WMS tile servers" : "Niestandardowe kafelki serwerowe WMS",
    "WMS version" : "Wersja WMS",
    "Layers to display" : "Warstwy do wyświetlenia",
    "Your WMS tile servers" : "Twoje kafelki WMS serwerów",
    "Custom WMS overlay servers" : "Niestandardowe nakładki WMS serwerów",
    "Your WMS overlay tile servers" : "Twoje nakładki WMS kafelek serwerowych",
    "Features overview" : "Przegląd funkcji",
    "Shortcuts" : "Skróty",
    "toggle sidebar" : "rozwiń boczne menu",
    "Documentation" : "Dokumentacja",
    "Source management" : "Zarządzanie źródłem",
    "Authors" : "Autorzy"
},
"nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);");
