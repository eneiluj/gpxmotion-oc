# GpxMotion Nextcloud application

GpxMotion is a Nextcloud application to create and display path animations on an interactive map.

To watch an animation, click the "Load and view file" button in GpxMotion main page. If no animation information was set, a default one (one section of 10 seconds per tracks/routes) is shown. If no time information is missing in an animation section and the "use real time proportions" is enabled (by default, it is), the animation duration will not change but the animation speed will be proportional to real speed.

To define an animation, go to GpxMotion main page and load a GPX file already containing ordered tracks/routes. Then define the animation steps. Then check you are satisfied with the animation preview. Then save the result to a GPX file (the animation data is saved as JSON in the GPX description field).

If a file is publicly shared without password in "Files" app, you can produce a GpxMotion public link to its animation with the "Share" button in the "view" page.

This app is tested on Nextcloud 16 with Firefox and Chromium.

Any feedback will be appreciated.

Go to [GpxMotion Crowdin project](https://crowdin.com/project/gpxmotion) if you want to help to translate this app in your language.

## Installation

Check the [AdminDoc](https://gitlab.com/eneiluj/gpxmotion-oc/wikis/admindoc) for installation details and integration in "Files" app.

## Alternatives

If you look for alternatives, take a look at :
- [gpxanim](https://github.com/rvl/gpxanim) creates a video file
- [GPX Animator](http://zdila.github.io/gpx-animator/) creates a video file


