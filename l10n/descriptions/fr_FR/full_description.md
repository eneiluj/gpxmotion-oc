# Application Nextcloud GpxMotion

GpxMotion est une application Nextcloud pour créer et afficher des animations sur une carte interactive.

Pour regarder une animation, cliquez sur le bouton « Charger et voir le fichier » dans la page principale de GpxMotion. Si aucune information d'animation n'a été définie, une animation par défaut (une section de 10 secondes par trace/route) est affichée. Si aucune information de temps n'est manquante dans une section d'animation et que "utiliser les proportions en temps réel" est activé (activé par défaut), la durée de l'animation ne changera pas mais la vitesse de l'animation sera proportionnelle à la vitesse réelle.

Pour définir une animation, allez sur la page principale de GpxMotion et chargez un fichier GPX contenant déjà des traces/routes ordonnées. Définissez ensuite les étapes de l'animation. Vérifiez ensuite que vous êtes satisfait avec l'aperçu de l'animation. Enregistrez ensuite le résultat dans un fichier GPX (les données d'animation sont enregistrées en JSON dans le champ de description GPX).

Si un fichier est partagé publiquement sans mot de passe dans l'application "Fichiers", vous pouvez produire un lien public GpxMotion vers son animation avec le bouton "Partager" dans la page "vue".

Cette application est testée sur Nextcloud 16 avec Firefox et Chromium.

Tout retour sera apprécié.

Allez sur [le projet GpxMotion sur Crowdin](https://crowdin.com/project/gpxmotion) si vous voulez aider à traduire cette application dans votre langue.

## Installation

Voir l' [AdminDoc](https://gitlab.com/eneiluj/gpxmotion-oc/wikis/admindoc) pour les détails sur l'installation.

## Alternatives

Si vous cherchez des alternatives, jetez un coup d'œil à :
- [gpxanim](https://github.com/rvl/gpxanim) crée un fichier vidéo
- [Animateur GPX](http://zdila.github.io/gpx-animator/) crée un fichier vidéo


