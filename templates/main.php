<?php
script('gpxmotion', 'gpxMotionEdit');

style('gpxmotion', 'fontawesome-free/css/all.min');
style('gpxmotion', 'edit');
style('gpxmotion', 'style');

?>

<div id="app">
    <div id="app-content">
        <?php print_unescaped($this->inc('editcontent')); ?>
    </div>
</div>
